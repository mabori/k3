import java.util.Iterator;
import java.util.LinkedList;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class DoubleStack {

   public static void main (String[] argum) {
      DoubleStack ds = new DoubleStack();
      System.out.println("empty\ttrue\t" + ds.stEmpty());
      for (int i = 1; i <= 10; i++) {
         ds.push(0.1 * i);
      }
      System.out.println("last\t1.0\t" + ds.pop());
      DoubleStack ds2 = null;
      try {
         ds2 = (DoubleStack) ds.clone();
      } catch (CloneNotSupportedException e) {e.printStackTrace();}
      System.out.println("copy\t0.9\t" + (ds2 != null ? ds2.pop() : "null"));
      ds.pop();
      System.out.println("equals\ttrue\t" + ds.equals(ds2));
      System.out.println(ds);

   }
   private final LinkedList<Double> ll;

   DoubleStack() {
      ll = new LinkedList<>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      DoubleStack result = new DoubleStack();
      for (Iterator<Double> it = ll.descendingIterator(); it.hasNext(); ) {
         result.push(it.next());
      }
      return result;
   }

   public boolean stEmpty() {
      return ll.isEmpty();
   }

   public void push (double a) {
      ll.push(a);
   }

   public double pop() {
       return ll.removeFirst();
   }

   public void op (String s) {
      double el2 = this.pop();
      double el1 = this.pop();
      double result;
      switch (s) {
         case "+": {
            result = el1 + el2;
            break;
         }
         case "-": {
            result = el1 - el2;
            break;
         }
         case "*": {
            result = el1 * el2;
            break;
         }
         case "/": {
            result = el1 / el2;
            break;
         }
         default: {
            result = el1;
         }
      }
      this.push(result);
   }

   public double tos() {
      return ll.getFirst();
   }

   @Override
   public boolean equals (Object o) {
      if (o instanceof DoubleStack) {
         DoubleStack other = (DoubleStack) o;

         if (this.stEmpty()) {
            return other.stEmpty();
         }

         DoubleStack thisClone, otherClone;
         try {
            thisClone = (DoubleStack) this.clone();
            otherClone = (DoubleStack) other.clone();
         } catch (CloneNotSupportedException e) {
            return false;
         }

         while (!thisClone.stEmpty()) {
            if (otherClone.stEmpty() || thisClone.pop() != otherClone.pop()) {
               return false;
            }
         }
         return true;
      }
      return false;
   }

   @Override
   public String toString() {
      return "DoubleStack: {" +
              ll.stream().map(Object::toString)
                      .collect(Collectors.joining(", ")) +
              "}";
   }

   public static double interpret (String pol) {
      String[] splitPol = pol.trim().split("\\s+");
      DoubleStack stack = new DoubleStack();

      // Below is regex suggested by Double.valueOf(String) documentation.
      final String Digits     = "(\\p{Digit}+)";
      final String HexDigits  = "(\\p{XDigit}+)";
      final String Exp        = "[eE][+-]?"+Digits;
      final String fpRegex    =
          ("[\\x00-\\x20]*"+
          "[+-]?(" +
          "NaN|" +
          "Infinity|" +
          "((("+Digits+"(\\.)?("+Digits+"?)("+Exp+")?)|"+

          "(\\.("+Digits+")("+Exp+")?)|"+

          "((" +
          "(0[xX]" + HexDigits + "(\\.)?)|" +

          "(0[xX]" + HexDigits + "?(\\.)" + HexDigits + ")" +

          ")[pP][+-]?" + Digits + "))" +
          "[fFdD]?))" +
          "[\\x00-\\x20]*");

      for (String s : splitPol) {
         if (Pattern.matches(fpRegex, s)) {
            stack.push(Double.valueOf(s)); // Will not throw NumberFormatException
         } else {
            switch (s) {
               case "+":
               case "-":
               case "*":
               case "/": {
                  stack.op(s);
                  break;
               }
               default:
                  throw new RuntimeException("Operator not supported.");
            }
         }
      }
      double result = stack.pop();
      if (stack.stEmpty()) {
         return result;
      } else {
         throw new RuntimeException("Expression is not correct.");
      }
   }
}

